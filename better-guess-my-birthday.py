from random import randint
user_name = input("Hi! What is your name? ")
number_of_guesses = 5

for guess in range(number_of_guesses):
    current_guess = guess + 1
    month = randint(1, 12)
    year = randint(1924, 2004)
    print("Guess", current_guess, ":", user_name, "were you born in", month, "/", year, "?")
    answer = input("yes or no? ")
    if answer == "yes":
        print("I knew it!")
        exit()
    elif guess == 4:
        print("I have other things to do. Good bye.")
    elif answer == "no":
        print("Drat! Lemme try again")
    else:
        print("Input is incorrect, please ensure all characters are lowercase")
